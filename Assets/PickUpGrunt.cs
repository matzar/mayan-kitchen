﻿using UnityEngine;
using System.Collections;

public class PickUpGrunt : MonoBehaviour {

	private AudioSource audioSource;

	void Start () {
		audioSource = GetComponent<AudioSource> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (!gameObject.activeInHierarchy)
			audioSource.Play ();
	}
}
