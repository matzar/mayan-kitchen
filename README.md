# Mayan-Kitchen
Global Game Jam 2016
url: http://globalgamejam.org/2016/games/mayan-kitchen

My first game jam and my first game in Unity with less than two weeks Unity experience.

Credits:

Cameron R. Home - Producer & Designer [@cameron_r_home](https://twitter.com/cameron_r_home)

Callum J. Allison - Lead Designer [@AllisonTowers](https://twitter.com/AllisonTowers)

Rob Small - Character Art [@TheWorstMuppet](https://twitter.com/TheWorstMuppet)

Kemal Thomson - Environment Art [@Alaister_Grier](https://twitter.com/Alaister_Grier)

Mateusz Zaremba - Programming [@jollyjellycoco](https://twitter.com/jollyjellycoco)

Jamie Wood - Programming Consultant [@jctwood](https://twitter.com/jctwood)

Chris D'Arcy - Audio [@chrisjdarcy](https://twitter.com/chrisjdarcy)

![Alt text](http://globalgamejam.org/sites/default/files/styles/game_sidebar__wide/public/game/featured_image/featuredimage_18.png?itok=N9qavmbl "Mayan Kitchen Credits")
